package connections

import (
	"crypto/rand"
	"cwtch.im/cwtch/model"
	"cwtch.im/cwtch/peer/peer"
	"cwtch.im/cwtch/protocol"
	"fmt"
	"git.openprivacy.ca/openprivacy/libricochet-go"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/connection"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"golang.org/x/crypto/ed25519"
	"net"
	"testing"
	"time"
)

func PeerAuthValid(hostname string, key ed25519.PublicKey) (allowed, known bool) {
	return true, true
}

func runtestpeer(t *testing.T, tp *TestPeer, identity identity.Identity, listenChan chan bool) {
	ln, _ := net.Listen("tcp", "127.0.0.1:5452")
	listenChan <- true
	conn, _ := ln.Accept()
	defer conn.Close()

	rc, err := goricochet.NegotiateVersionInbound(conn)
	if err != nil {
		t.Errorf("Negotiate Version Error: %v", err)
	}
	err = connection.HandleInboundConnection(rc).ProcessAuthAsV3Server(identity, PeerAuthValid)
	if err != nil {
		t.Errorf("ServerAuth Error: %v", err)
	}
	tp.RegisterChannelHandler("im.cwtch.peer", func() channels.Handler {
		cpc := new(peer.CwtchPeerChannel)
		cpc.Handler = tp
		return cpc
	})

	rc.Process(tp)
}

type TestPeer struct {
	connection.AutoConnectionHandler
	ReceivedIdentityPacket bool
	ReceivedGroupInvite    bool
}

func (tp *TestPeer) HandleGroupInvite(gci *protocol.GroupChatInvite) {
	tp.ReceivedGroupInvite = true
}

func TestPeerPeerConnection(t *testing.T) {
	pub, priv, _ := ed25519.GenerateKey(rand.Reader)
	identity := identity.InitializeV3("", &priv, &pub)

	profile := model.GenerateNewProfile("alice")
	hostname := identity.Hostname()
	ppc := NewPeerPeerConnection(connectivity.LocalProvider(), "127.0.0.1:5452|"+hostname, profile, nil, application.ApplicationInstanceFactory{})

	tp := new(TestPeer)
	tp.Init()
	listenChan := make(chan bool)
	go runtestpeer(t, tp, identity, listenChan)
	<-listenChan
	state := ppc.GetState()
	if state != DISCONNECTED {
		fmt.Println("ERROR state should be disconnected")
		t.Errorf("new connections should start in disconnected state")
	}
	go ppc.Run()
	time.Sleep(time.Second * 5)
	state = ppc.GetState()
	if state != AUTHENTICATED {
		t.Errorf("connection state should be authenticated(3), was instead %v", state)
	}
	_, invite, _ := profile.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	ppc.SendGroupInvite(invite)
	time.Sleep(time.Second * 3)
	if tp.ReceivedGroupInvite == false {
		t.Errorf("should have received an group invite packet")
	}
}
