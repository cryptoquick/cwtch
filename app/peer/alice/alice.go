package main

import (
	"cwtch.im/cwtch/peer"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
)

func main() {
	log.AddEverythingFromPattern("peer/alice")
	alice := peer.NewCwtchPeer("alice")

	processData := func(onion string, data []byte) []byte {
		log.Debugf("Recieved %s from %v", data, onion)
		return data
	}

	alice.SetPeerDataHandler(processData)
	alice.Listen()

}
