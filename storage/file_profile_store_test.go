package storage

import (
	"cwtch.im/cwtch/peer"
	"testing"
)

func TestFileProfileStore(t *testing.T) {
	fileStore := CreateFileProfileStore(".test.json", "password")
	alice := peer.NewCwtchPeer("alice")
	fileStore.Save(alice)

	aliceLoaded, err := fileStore.Load()

	if err != nil {
		t.Errorf("alice profile should have been loaded from store instead %v", err)
	}

	if aliceLoaded.GetProfile().Name != "alice" {
		t.Errorf("alice profile should have been loaded from store instead %v", aliceLoaded)
	}
}
